PREFIX := ~/.local
CONFIGDIR := ~/.config

install:
	mkdir -p $(PREFIX)/bin
	install -m 0700 mrandr $(PREFIX)/bin/mrandr
	cp -f mrandr-xrandr-parser.sed $(PREFIX)/bin/

uninstall:
	rm -rf $(PREFIX)/bin/mrandr $(PREFIX)/bin/mrandr-xrandr-parser.sed
